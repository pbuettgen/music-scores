<!--
SPDX-FileCopyrightText: 2016 Philipp Büttgenbach

SPDX-License-Identifier: CC-BY-SA-4.0
-->

# music-scores

This is my collection of music scores which can also be found on
[imslp.org](https://imslp.org/wiki/Category:B%C3%BCttgenbach,_Philipp).
