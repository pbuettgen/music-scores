#! /usr/bin/python
# -*- coding: utf-8 -*-

# SPDX-FileCopyrightText: 2022 Philipp Büttgenbach
#
# SPDX-License-Identifier: Apache-2.0

composers = {
    "Arthur Seybold": {
        "born": 1868,   "died": 1948
    },
    "Jean Baptiste Charles Dancla": {
        "born": 1817,   "died": 1907
    },
    "Ferdinand Küchler": {
        "born": 1867,   "died": 1937
    },
    "Franz Wohlfahrt": {
        "born": 1833,   "died": 1884
    },
    "Friedrich Seitz": {
        "born": 1848,   "died": 1918
    },
    "Heinrich Ernst Kayser": {
        "born": 1815,   "died": 1888
    },
    "Ignace Pleyel": {
        "born": 1757,   "died": 1831
    },
    "Jaques-Féréol Mazas": {
        "born": 1782,   "died": 1849
    },
    "Johann Wenzel Kalliwoda": {
        "born": 1801,   "died": 1866
    },
    "Leo Portnoff": {
        "born": 1875,   "died": 1940
    },
    "Oskar Rieding": {
        "born": 1840,   "died": 1918
    }
}


if "__main__" == __name__:
    import json
    with open("composers.json", "w") as f:
        json.dump(composers, f)
