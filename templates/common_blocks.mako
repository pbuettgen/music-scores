## -*- coding: utf-8 -*-
%% -*- coding: utf-8 -*-

<%doc>
SPDX-FileCopyrightText: 2021 Philipp Büttgenbach

SPDX-License-Identifier: CC-BY-SA-4.0
</%doc>

<%def name="lilypond_preamble()">

\version "2.24"

\include "lily-snippets.ily"
\include "tagline.ily"
\include "copyright.ily"
\include "paper.ily"
</%def>

<%def name="global_header_block(xml_root)">
% if xml_root.find('./preamble') is not None:
  ${xml_root.find('./preamble').text}
% endif
\header {
% if xml_root.find('./title') is not None:
  title = "${xml_root.find('./title').text}"
% endif
% if xml_root.find('./subtitle') is not None:
  subtitle = "${xml_root.find('./subtitle').text}"
% endif
% if xml_root.find('./opus') is not None:
  opus = "${xml_root.find('./opus').text}"
% endif
% if xml_root.find('./arranger') is not None:
  arranger = "${xml_root.find('./arranger').text}"
% endif
% if xml_root.find('./composer') is not None:
<%
  from composers import composers
  composer_name = xml_root.find('./composer').text
  composer_string = "{0} ({1} – {2})".format(
    composer_name,
    composers[composer_name]['born'],
    composers[composer_name]['died']
  ) if composer_name in composers.keys() else composer_name
%>
  composer = "${composer_string}"
  pdfauthor = "${composer_name}"
% endif
}
</%def>

<%def name="add_quotes(xml_root)">
% for labeled in xml_root.findall('.//*[@label]'):
\addQuote "${labeled.attrib['label']}" {
  ${labeled.text}
}
% endfor
</%def>
