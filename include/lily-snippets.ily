%%% -*- coding: utf-8 -*-

% SPDX-FileCopyrightText: 2016 Philipp Büttgenbach
%
% SPDX-License-Identifier: CC-BY-SA-4.0

\version "2.22"

\include "dynamicH.ily"

%%
%% Custom annotations
%%
agitato = \markup \italic #"agitato"
allargando = \markup \italic #"allargando"
attacca = \markup \italic #"attacca"
brillante = \markup \italic #"brillante"
aTempo = \markup \italic #"a tempo"
cadenzaAdLib = \markup \italic #"Cadenza ad lib."
cantabile = \markup \italic #"cantabile"
dolce = \markup \italic #"dolce"
dolceWhiteOut = \markup \whiteout {
  \override #'(style . outline)
  \override #'(thickness . 3)
  \italic #"dolce"
}
energico = \markup \italic #"energico"
espr = \markup \italic #"espressivo"
espressione = \markup \italic #"espressione"
grazioso = \markup \italic #"grazioso"
leggiero = \markup \italic #"leggiero"
marcatoBasso = \markup \italic #"marcato basso"
molto = \markup \italic #"molto"
moltoRit = \markup \italic #"molto rit."
morendo = \markup \italic #"morendo"
morendoWhiteOut = \markup {
  \override #'(style . outline)
  \override #'(thickness . 3)
  \whiteout\italic #"morendo"
}
piuMoto = \markup \italic #"più moto"
pocoPocoMorendo = \markup \italic "poco a poco morendo"
pocoRit = \markup \italic #"poco rit."
rall = \markup \italic #"rall."
risoluto = \markup \italic #"risoluto"
rit = \markup \italic #"rit."
sempreF = \markup {\italic #"sempre" \dynamic #"f"}
senzaAllargando = \markup \italic #"senza allargando"
simile = \markup \italic #"simile"
solo = \markup {
  \override #'(font-name . "Piazzolla SC Italic")
  "Solo."
}
sostenuto = \markup \italic #"sostenuto"
stringendo = \markup \italic #"stringendo"
tenutoTxt = \markup \italic #"tenuto"
tranquillo= \markup \italic #"tranquillo"
tutti = \markup {
  \override #'(font-name . "Piazzolla SC Italic")
  "Tutti."
}

%%
%% Custom tempo marks
%%
tempoAgitato = \markup {#"Agitato."}
tempoLento = \markup {\italic "lento"}
tempoPrimo = \markup {#"Tempo 1°"}

%%
%% Custom dynamics
%%
fAgitato = \dynamicH 1 "{f} agitato"
fEnergico = \dynamicH 1 "{f} energico"
fRisoluto = \dynamicH 1 "{f} risoluto"
pMaEspressivo = \dynamicH 1 "{p} ma espressivo"
pMarcato = \dynamicH 1 "{p} marcato"
pDolce = \dynamicH 1 "{p} dolce"
pDolceGrazioso = \dynamicH 1 "{p} dolce, grazioso"
rf = #(make-dynamic-script "rf")

pocoPocoStringCresc = #(make-music 'CrescendoEvent
              'span-direction START
              'span-type 'text
              'span-text "poco a poco string. e cresc.")

pocoDimWhiteoutTxt = \markup{
  \override #'(style . outline)
  \override #'(thickness . 3)
  \whiteout \italic "poco dim."
}

pocoDimWhiteout = #(make-music 'CrescendoEvent
              'span-direction START
              'span-type 'text
              'span-text pocoDimWhiteoutTxt)

pocoDim = #(make-music 'CrescendoEvent
              'span-direction START
              'span-type 'text
              'span-text "poco dim.")

crescWhiteOutTxt = \markup{
  \override #'(style . outline)
  \override #'(thickness . 3)
  \whiteout \italic "cresc."
}

crescWhiteOut = #(make-music 'CrescendoEvent
              'span-direction START
              'span-type 'text
              'span-text crescWhiteOutTxt)

dimWhiteOutTxt = \markup{
  \override #'(style . outline)
  \override #'(thickness . 3)
  \whiteout \italic "dim."
}

dimWhiteOut = #(make-music 'CrescendoEvent
              'span-direction START
              'span-type 'text
              'span-text dimWhiteOutTxt)

pocoCresc = #(make-music 'CrescendoEvent
              'span-direction START
              'span-type 'text
              'span-text "poco cresc.")

pocoCrescWhiteOutTxt = \markup{
  \override #'(style . outline)
  \override #'(thickness . 3)
  \whiteout \italic "poco cresc."
}

pocoCrescWhiteOut = #(make-music 'CrescendoEvent
              'span-direction START
              'span-type 'text
              'span-text pocoCrescWhiteOutTxt)

pocoAPocoCresc = #(make-music 'CrescendoEvent
                   'span-direction START
                   'span-type 'text
                   'span-text "poco a poco cresc.")

pocoAPocoCrescWhiteOutTxt = \markup{
  \override #'(style . outline)
  \override #'(thickness . 3)
  \whiteout \italic "poco a poco cresc."
}

pocoAPocoCrescWhiteOut = #(make-music 'CrescendoEvent
              'span-direction START
              'span-type 'text
              'span-text pocoAPocoCrescWhiteOutTxt)

ffWhiteOutTxt = \markup {
  \override #'(style . outline)
  \override #'(thickness . 3)
  \whiteout \dynamic #"ff"
}
ffWhiteOut = #(make-dynamic-script ffWhiteOutTxt)
fWhiteOutTxt = \markup {
  \override #'(style . outline)
  \override #'(thickness . 3)
  \whiteout \dynamic "f"
}
fWhiteOut = #(make-dynamic-script fWhiteOutTxt)
mfWhiteOutTxt = \markup {
  \override #'(style . outline)
  \override #'(thickness . 3)
  \whiteout \dynamic "mf"
}
mfWhiteOut = #(make-dynamic-script mfWhiteOutTxt)
ppWhiteOutTxt = \markup {
  \override #'(style . outline)
  \override #'(thickness . 3)
  \whiteout \dynamic "pp"
}
ppWhiteOut = #(make-dynamic-script ppWhiteOutTxt)
pWhiteOutTxt = \markup {
  \override #'(style . outline)
  \override #'(thickness . 3)
  \whiteout \dynamic "p"
}
pWhiteOut = #(make-dynamic-script pWhiteOutTxt)

%% Other commands

ritSpan = {
  \override TextSpanner.bound-details.left.text = \rit
  \override TextSpanner.bound-details.left.stencil-align-dir-y = #CENTER
}

prallNatural = \markup\left-align{\musicglyph #"scripts.prall" \super \natural}
trillFlat = \markup { \musicglyph #"scripts.trill" \super \flat }
trillSharp =  \markup{\musicglyph #"scripts.trill" \super \sharp}
trillNatural = \markup{\musicglyph #"scripts.trill" \super \natural}

#(define-markup-command (columns layout props args) (markup-list?)
   (let ((line-width (/ (chain-assoc-get 'line-width props
                         (ly:output-def-lookup layout 'line-width))
                        (max (length args) 1))))
     (interpret-markup layout props
       (make-line-markup (map (lambda (line)
                                (markup #:pad-to-box `(0 . ,line-width) '(0 . 0)
                                  #:override `(line-width . ,line-width)
                                  line))
                               args)))))

hairpinWithCenteredText =
#(define-music-function (parser location text) (markup?)
  #{
  \once \override Voice.Hairpin.after-line-breaking =
  #(lambda (grob)
    (let* ((stencil (ly:hairpin::print grob))
	   (par-y (ly:grob-parent grob Y))
	   (dir (ly:grob-property par-y 'direction))
	   (new-stencil (ly:stencil-aligned-to
			 (ly:stencil-combine-at-edge
			  (ly:stencil-aligned-to stencil X CENTER)
			  Y dir
			  (ly:stencil-aligned-to (grob-interpret-markup grob text) X CENTER))
			 X LEFT))
	   (staff-space (ly:output-def-lookup (ly:grob-layout grob) 'staff-space))
	   (staff-line-thickness
	    (ly:output-def-lookup (ly:grob-layout grob) 'line-thickness))
	   (grob-name (lambda (x) (assq-ref (ly:grob-property x 'meta) 'name)))
	   (par-x (ly:grob-parent grob X))
	   (dyn-text (eq? (grob-name par-x) 'DynamicText ))
	   (dyn-text-stencil-x-length
	    (if dyn-text
	     (interval-length
	      (ly:stencil-extent (ly:grob-property par-x 'stencil) X))
	     0))
	   (x-shift
	    (if dyn-text
	     (-
	      (+ staff-space dyn-text-stencil-x-length)
	      (* 0.5 staff-line-thickness)) 0)))

     (ly:grob-set-property! grob 'Y-offset 0)
     (ly:grob-set-property! grob 'stencil
      (ly:stencil-translate-axis
       new-stencil
       x-shift X))))
  #})

hairpinCalmato =
\hairpinWithCenteredText \markup { \italic "calmato" }
hairpinDolce =
\hairpinWithCenteredText \markup { \italic "dolce" }

fingerNumberSpanner =
#(define-music-function (parser location FingerNumber) (string?)
  #{
  \override TextSpanner.style = #'solid
  \override TextSpanner.bound-details.left.stencil-align-dir-y = #CENTER
  \override TextSpanner.bound-details.left.text = \markup { \finger #FingerNumber }
  #})

%%% Local Variables:
%%% coding: utf-8
%%% eval: (LilyPond-mode)
%%% End:
