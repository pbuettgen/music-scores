%%% -*- coding: utf-8 -*-

% SPDX-FileCopyrightText: 2019 Philipp Büttgenbach
%
% SPDX-License-Identifier: CC-BY-SA-4.0

\paper {
  #(set-paper-size "c4")

  #(define fonts
    (set-global-fonts
     #:roman "Piazzolla"
     #:sans "Montserrat"
   ))

  binding-offset = 5\mm
  page-breaking = #ly:page-turn-breaking
  two-sided = ##t
  auto-first-page-number = ##t
  blank-after-score-page-penalty = #.1
  number-of-divisions = 21
  division-width  = #(/ (- paper-width binding-offset) number-of-divisions)
  division-height = #(/ paper-height number-of-divisions)
  inner-margin  = #(* 1 division-width)
  outer-margin  = #(* 2 division-width)
  top-margin    = #(* 1 division-height)
  bottom-margin = #(* 2 division-height)
  line-width =  #(- (- paper-width (* 3 division-width)) binding-offset)

  bookTitleMarkup = \markup {
    \override #'(baseline-skip . 3.5)
    \column {
      \fill-line { \fromproperty #'header:dedication }
      \override #'(baseline-skip . 4)
      \column {
        \fill-line {
          \abs-fontsize #20 \bold \sans \fromproperty #'header:title
        }
        \fill-line {
          \override #'(font-name . "Montserrat SemiBold")
          \abs-fontsize #14 \fromproperty #'header:subtitle
        }
        \fill-line {
          \smaller \bold
          \fromproperty #'header:subsubtitle
        }
        \fill-line {
          \fromproperty #'header:poet
          { \override #'(font-name . "Montserrat SemiBold")
            \larger \fromproperty #'header:instrument }
          \fromproperty #'header:composer
        }
        \fill-line {
          \fromproperty #'header:meter
          \fromproperty #'header:arranger
        }
        \fill-line {
          ""
          \fromproperty #'header:opus
        }
      }
    }
  }

  scoreTitleMarkup = \markup \column {
    \if \should-print-all-headers { \bookTitleMarkup \hspace #1 }
    \override #'(font-name . "Montserrat Medium")
    \larger \fromproperty #'header:piece
  }

  oddHeaderMarkup = \markup \overlay {
    \override #'(font-name . "Montserrat Medium")
    \fill-line {
      ""
      \unless \on-first-page-of-part \fromproperty #'header:instrument
      \if \should-print-page-number
        \sans \fromproperty #'page:page-number-string
    }
    \unless \on-first-page-of-part \lower #1 \draw-hline
  }

  evenHeaderMarkup = \markup \overlay {
    \override #'(font-name . "Montserrat Medium")
    \fill-line {
      \if \should-print-page-number
        \sans \fromproperty #'page:page-number-string
      \unless \on-first-page-of-part \fromproperty #'header:instrument
      ""
    }
    \unless \on-first-page-of-part \lower #1 \draw-hline
  }

  top-system-spacing.padding = 2
  %% TODO: Should be zero on first page!
  top-markup-spacing.padding = 3
  system-system-spacing.stretchability = 24
}

%%% Thanks at lot to Harm@lilypondforum.de for these lines of code.
#(define (my-format-metronome-markup event context)
  (let ((hide-note (ly:context-property context 'tempoHideNote #f))
        (text
         #{
         \markup
         \override #'(font-name . "Piazzolla SemiBold")
         #(ly:event-property event 'text)
         #})
(dur (ly:event-property event 'tempo-unit))
(count (ly:event-property event 'metronome-count)))
((@@ (lily) metronome-markup) text dur count hide-note)))

\layout {
  \context {
    \Score
    metronomeMarkFormatter = #my-format-metronome-markup
  }
  \override Score.BarNumber.font-name = #"Piazzolla Light"
}

\layout {
  \context {
    \Staff
    ottavationMarkups = #ottavation-ordinals
  }
}

%%% Local Variables:
%%% eval: (LilyPond-mode)
%%% End:
