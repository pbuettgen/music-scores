%%% -*- coding: utf-8 -*-

% SPDX-FileCopyrightText: 2016 Philipp Büttgenbach
%
% SPDX-License-Identifier: CC-BY-SA-4.0

\version "2.20"

\header {
  copyright = \markup \overlay {
    \raise #1.6 \draw-hline
    \fill-line \general-align #Y #CENTER {
      \tiny \sans #(strftime "Copyright © %Y Philipp Büttgenbach"
                    (localtime (current-time)))
      \with-url #"http://creativecommons.org/licenses/by-sa/4.0/"
      \override #'(thickness . 2) \override #'(box-padding . .3) \rounded-box #"🅭 🅯 🄎"
    }
  }
}
