# -*- coding: utf-8 -*-

# SPDX-FileCopyrightText: 2012 https://github.com/mucius
# SPDX-FileCopyrightText: 2022 Philipp Büttgenbach
#
# SPDX-License-Identifier: Apache-2.0

"""SCons.Tool.lilypond

Tool-specific initialization for LilyPond.

There normally shouldn't be any need to import this module directly.
It will usually be imported through the generic SCons.Tool.Tool()
selection method.
"""

from cchardet import detect as char_detect
from pathlib import Path as pl_Path
from re import compile as re_compile
from SCons.Util import CLVar
import SCons.Action, SCons.Builder, SCons.Scanner


class _lyScanner:
    """ lilypond scanner """

    def __init__(self):
        self._include_re = re_compile(r'\s*\\include\s+"(\S+)".*')

    def __call__(self, node, env, path):
        contents = node.get_contents()
        if not contents:
            return []
        encoding = char_detect(contents)['encoding']
        includes = self._include_re.findall(
            contents.decode(encoding if encoding is not None else ''))
        results = [env.FindFile(inc, path) for inc in includes]
        return [x for x in results if x is not None]


def generate(env):
    """ generate builder """

    env.SetDefault(
        INCPREFIX='-I',
        INCSUFFIX='',
        LYCOM='$LY $LYFLAGS $_LYINCFLAGS -o ${TARGET.dir} ${SOURCE}',
        LYCOMSTR="",
        LYFLAGS=CLVar('--loglevel=ERROR') if 1 < env.GetOption('num_jobs') else CLVar(''),
        _LYINCFLAGS='${_concat(INCPREFIX, LYPATH, INCSUFFIX, __env__)}',
        LY='lilypond',
        LYPATH=[]
    )
    # required by fontconfig
    env['ENV']['HOME'] = pl_Path.home()

    scanner = env.Scanner(
        function=_lyScanner(),
        name='Lilypond Scanner', skeys=['.ly', '.ily'],
        path_function=SCons.Scanner.FindPathDirs('LYPATH'),
        recursive=1)
    env['SCANNERS'] += [scanner]
    env['BUILDERS']['LilyPond'] = SCons.Builder.Builder(
        action=SCons.Action.Action("$LYCOM", "$LYCOMSTR"),
        suffix='.pdf', src_suffix='.ly')


def exists(env):
    """ existance check """
    return env.Detect('lilypond')


# Local Variables:
# tab-width:4
# indent-tabs-mode:nil
# End:
# vim: set expandtab tabstop=4 shiftwidth=4:
