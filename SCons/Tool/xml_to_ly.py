# -*- coding: utf-8 -*-

# SPDX-FileCopyrightText: 2022 Philipp Büttgenbach
#
# SPDX-License-Identifier: Apache-2.0

"""SCons.Tool.xml_to_ly

Tool-specific initialization for xmlToLy.

There normally shouldn't be any need to import this module directly.
It will usually be imported through the generic SCons.Tool.Tool()
selection method.
"""

import sys
from mako.lookup import TemplateLookup
from pathlib import Path as pl_Path
import xml.etree.ElementTree as ET
from SCons.Builder import Builder


def _xml_to_ly(target, source, env):
    """Convert xml to Lilypond."""

    template_dir = str(env['MAKO_TEMPLATE_DIR'].resolve())

    if template_dir not in sys.path:
        sys.path.append(template_dir)

    xml_tree = ET.parse(str(source[0]))
    xml_root = xml_tree.getroot()

    template_lookup = TemplateLookup(directories=template_dir)

    template_name = xml_root.tag + ".mako"

    template = template_lookup.get_template(template_name)

    with open(str(target[0]), "w") as tf:
        tf.write(template.render(xml_root=xml_root))


def generate(env):
    env.SetDefault(MAKO_TEMPLATE_DIR=pl_Path("."))
    env['BUILDERS']['xmlToLy'] = Builder(
        action=_xml_to_ly,
        suffix='.ly',
        src_suffix='.xml')


def exists(env):
    """ existance check """
    return True


# Local Variables:
# tab-width:4
# indent-tabs-mode:nil
# End:
# vim: set expandtab tabstop=4 shiftwidth=4:
